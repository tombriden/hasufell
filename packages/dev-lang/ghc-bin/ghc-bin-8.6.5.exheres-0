# Copyright 2018 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require bash-completion alternatives

SUMMARY="The Glorious Glasgow Haskell Compilation System"
DESCRIPTION="
GHC is a state-of-the-art, open source, compiler and interactive environment for the functional
language Haskell."
HOMEPAGE="https://www.haskell.org/ghc/"
DOWNLOADS="https://downloads.haskell.org/~ghc/${PV}/ghc-${PV}-x86_64-fedora27-linux.tar.xz"

LICENCES="BSD-3"
SLOT="${PV}"
PLATFORMS="-* ~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/perl:*[>=5.6.1]
        dev-libs/gmp[>=6.1] [[ note = [ depends on libgmp.so.10 ] ]]
        sys-libs/ncurses[>=6.1-r2] [[ note = [ Only these include libtinfo.so symlinks ] ]]
        !dev-lang/GHC [[
            description = [ different provider ]
            resolution = uninstall-blocked-after
        ]]
        !dev-lang/ghc [[
            description = [ Both install the same binaries/libs and alternatives ]
            resolution = uninstall-blocked-before
        ]]
    suggestion:
        dev-haskell/cabal-install-bin [[ description = [ the cabal binary for installing packages ] ]]
        dev-lang/llvm[>=2.7] [[ description = [ LLVM can be used for code generation and linking ] ]]
"

BUGS_TO="hasufell@posteo.de"
REMOTE_IDS="freecode:${PN}"
UPSTREAM_RELEASE_NOTES="http://www.haskell.org/${PN}/docs/${PV}/html/users_guide/release-${PV//./-}.html"

RESTRICT="test"

WORK=${WORKBASE}/ghc-${PV}

src_configure() {
    econf \
        --target=$(exhost --target)
}

src_compile() {
    :
}

src_install() {
    default

    # bashcomp
    dobashcompletion "${FILES}/ghc-bash-completion"
    alternatives_for ghc ${SLOT} ${SLOT} \
        /usr/share/bash-completion/completions/ghc-bin{,-${SLOT}}

    # some scripts are not versioned, move
    edo mv "${IMAGE}"/usr/$(exhost --target)/bin/{hp2ps,hp2ps-${SLOT}}
    edo mv "${IMAGE}"/usr/$(exhost --target)/bin/{hpc,hpc-${SLOT}}
    edo mv "${IMAGE}"/usr/$(exhost --target)/bin/{hsc2hs,hsc2hs-${SLOT}}

    # remove unversioned symlinks, we create alternatives later
    local ghc_bin
    for ghc_bin in ghc ghci ghc-pkg haddock runghc runhaskell ; do
        edo rm "${IMAGE}"/usr/$(exhost --target)/bin/${ghc_bin}
    done
    unset ghc_bin

    # create alternatives for scripts/binaries
    alternatives_for ghc ${SLOT} ${SLOT} \
        /usr/$(exhost --target)/bin/ghc{,-${SLOT}} \
        /usr/$(exhost --target)/bin/ghci{,-${SLOT}} \
        /usr/$(exhost --target)/bin/ghc-pkg{,-${SLOT}} \
        /usr/$(exhost --target)/bin/haddock{,-ghc-${SLOT}} \
        /usr/$(exhost --target)/bin/hp2ps{,-${SLOT}} \
        /usr/$(exhost --target)/bin/hpc{,-${SLOT}} \
        /usr/$(exhost --target)/bin/hsc2hs{,-${SLOT}} \
        /usr/$(exhost --target)/bin/runghc{,-${SLOT}} \
        /usr/$(exhost --target)/bin/runhaskell /usr/$(exhost --target)/bin/runghc-${SLOT}

    # alternatives for manpages
    local file alternatives=()
    for file in "${IMAGE}"/usr/share/man/*/*; do
        alternatives+=(
            ${file#${IMAGE}}
            ${file#${IMAGE}}-${SLOT}
        )
    done
    alternatives_for \
        ghc ${SLOT} ${SLOT} \
        "${alternatives[@]}"
}
