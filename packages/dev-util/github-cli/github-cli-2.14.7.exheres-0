# Copyright 2020 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require bash-completion zsh-completion
require github [ user='cli' pn='cli' tag="v${PV}" ]

SUMMARY="Github CLI"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/go
    run:
        dev-scm/git
"

BUGS_TO="hasufell@posteo.de"

src_compile() {
    export GOPATH="${WORK}"/.gopath
    export GOBIN="${WORK}"/.bin
    export GH_VERSION="v${PV}"
    unset LDFLAGS

    esandbox disable_net
    emake
    emake manpages

    edo go run ./cmd/gh completion -s bash > gh.bash-completion
    edo go run ./cmd/gh completion -s zsh > _gh
    esandbox enable_net
}

src_install() {
    dobin bin/gh
    emagicdocs

    doman share/man/man?/gh*.?
    dobashcompletion gh.bash-completion gh
    dozshcompletion _gh
}

src_test() {
    esandbox disable_net
    default
    esandbox enable_net
}

