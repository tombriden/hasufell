# Copyright 2018 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user='polybar' release="${PV}" suffix=tar ] cmake [ api=2 ]

export_exlib_phases src_prepare

SUMMARY="A fast and easy-to-use status bar."

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    alsa
    curl [[ description = [ Support for github module ] ]]
    mpd
    network
    i3
    pulseaudio
    X
"

DEPENDENCIES="
    build+run:
        dev-lang/python:2.7
        x11-libs/cairo
        x11-libs/libxcb
        x11-proto/xcb-proto
        x11-utils/xcb-util-image
        x11-utils/xcb-util-wm
        alsa? (
            sys-sound/alsa-lib
        )
        curl? (
            net-misc/curl
        )
        mpd? (
            media-sound/mpd
        )
        pulseaudio? (
            media-sound/pulseaudio
        )
        network? (
            net-libs/libnl
        )
        X? (
            x11-proto/xcb-proto[>=1.12]
            x11-libs/libXrandr
            x11-utils/xcb-util-cursor
            x11-utils/xcb-util-xrm
        )
    recommendation:
        fonts/unifont
"

CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'alsa ALSA'
    'curl CURL'
    'i3 I3'
    'mpd MPD'
    'network NETWORK'
    'pulseaudio PULSEAUDIO'
)

CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'network LIBNL'
)

CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'X XCOMPOSITE'
    'X XCURSOR'
    'X XRANDR'
    'X XRANDR_MONITORS'
    'X XKB'
    'X XRM'
)

polybar_src_prepare() {
    edo mkdir "${TEMP}"/fakebin
    edo ln -s /usr/host/bin/python2 "${TEMP}"/fakebin/python
    export PATH="${TEMP}/fakebin/:${PATH}"

    default
}
